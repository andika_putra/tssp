%% TSSP for 3 components in 2D
% Numerical Simulations on Stationary States for 3 component BECs
% from Wang's thesis pg 113-- 
% Tested for Fig 5.2 and 5.3

clear all; close all; clc;

om_trap = 50;
hbar = 1.0545717e-34;
mRb = 1.4431606e-25;
a0 = sqrt(hbar/mRb/om_trap);
k_rec = 0;

Omega = 0.9;
Raman = 0;

gamma_x = 1;
gamma_y = 1.5;

delta_t = 0.01;	% time step interval

ax = -16; bx = 16;    % xmin and xmax
cy = -16; dy = 16;    % ymin and ymax

h = 1/10;           % h is the mesh size: h = (bx-ax)/Mx = (dy-cy)/Ny
Mx = round((bx-ax)/h);
Ny = round((dy-cy)/h);

x_j = (ax+h):h:(bx-h);
y_k = (cy+h):h:(dy-h);

% FFT summation indices for x and y spaces:
px = (-Mx/2+1):(Mx/2-1);
qy = (-Ny/2+1):(Ny/2-1);

% j_idx = 0:(Mx-1);
% m_idy = 0:(Ny-1);

% FFT reciprocal space
mu_p = 2*pi*px/(bx-ax);
la_q = 2*pi*qy/(dy-cy);

[yy,xx] = meshgrid(y_k,x_j);

% Initial wavefunction
% psi_ho = (gamma_x*gamma_y)^(1/4)/sqrt(pi).*exp(-(gamma_x*xx.^2+gamma_y*yy.^2)/2);   % xj * yk
% psi_v = (gamma_x*gamma_y)^(1/4)/sqrt(pi)*(xx+1i*yy).*exp(-(xx.^2+yy.^2)/2);         % xj * yk
% psi_cc = ((1)*psi_ho );
psi_cc = 1/sqrt(pi).*exp(-(xx.^2 + yy.^2)/2);   % xj * yk
norm_fac = sum(sum(abs(psi_cc).^2)*h)*h;

psi_1 = psi_cc/sqrt(norm_fac)*sqrt(0.05);
psi_2 = psi_cc/sqrt(norm_fac)*sqrt(0.9);
psi_3 = psi_cc/sqrt(norm_fac)*sqrt(0.05);

tTotal = round(10/delta_t);
N1 = zeros(1,tTotal);
N2 = zeros(1,tTotal);
N3 = zeros(1,tTotal);

delta1 = zeros(1,tTotal);
delta2 = zeros(1,tTotal);
delta3 = zeros(1,tTotal);

c0 = 100;
c2 = 2;

V2D_1 = 1/2*(gamma_x^2*xx.^2 + gamma_y^2*yy.^2); % xj * yk

for tt = 1:tTotal
    
    % First step
    psi_hat1 = FFT_x_sine(psi_1,Mx,Ny,x_j,mu_p,ax); % Ny * px
    psi_hat2 = FFT_x_sine(psi_2,Mx,Ny,x_j,mu_p,ax); % Ny * px
    psi_hat3 = FFT_x_sine(psi_3,Mx,Ny,x_j,mu_p,ax); % Ny * px

    psi_1_step = rotatingIFFT_x_mod(Omega,delta_t,psi_hat1,Mx,Ny,x_j,y_k,mu_p,ax); % yk * xj
    psi_2_step = rotatingIFFT_x_mod(Omega,delta_t,psi_hat2,Mx,Ny,x_j,y_k,mu_p,ax); % yk * xj
    psi_3_step = rotatingIFFT_x_mod(Omega,delta_t,psi_hat3,Mx,Ny,x_j,y_k,mu_p,ax); % yk * xj
    
    % Second step
    psi_hat1 = FFT_y_sine(psi_1_step.',Mx,Ny,y_k,la_q,cy); % Mx * qy
    psi_hat2 = FFT_y_sine(psi_2_step.',Mx,Ny,y_k,la_q,cy); % Mx * qy
    psi_hat3 = FFT_y_sine(psi_3_step.',Mx,Ny,y_k,la_q,cy); % Mx * qy
    
    psi_1_step = rotatingIFFT_y_mod(Omega,delta_t,psi_hat1,Mx,Ny,x_j,y_k,la_q,cy); % xj * yk
    psi_2_step = rotatingIFFT_y_mod(Omega,delta_t,psi_hat2,Mx,Ny,x_j,y_k,la_q,cy); % xj * yk
    psi_3_step = rotatingIFFT_y_mod(Omega,delta_t,psi_hat3,Mx,Ny,x_j,y_k,la_q,cy); % xj * yk
    
    % Third step
    [psi_1_stepNew,psi_2_stepNew,psi_3_stepNew] = PAPtransform(psi_1_step,psi_2_step,psi_3_step,c2,delta_t/2,0);    % xj * yk
    
    % Fourth step
    psi_1_step = exp(-1i*delta_t*(V2D_1 + c0*(abs(psi_1_stepNew).^2+abs(psi_2_stepNew).^2+abs(psi_3_stepNew).^2) + c2*(abs(psi_1_stepNew).^2+abs(psi_2_stepNew).^2-abs(psi_3_stepNew).^2))).*psi_1_stepNew;   % xj * yk
    psi_2_step = exp(-1i*delta_t*(V2D_1 + c0*(abs(psi_1_stepNew).^2+abs(psi_2_stepNew).^2+abs(psi_3_stepNew).^2) + c2*(abs(psi_1_stepNew).^2+abs(psi_3_stepNew).^2))).*psi_2_stepNew;   % xj * yk
    psi_3_step = exp(-1i*delta_t*(V2D_1 + c0*(abs(psi_1_stepNew).^2+abs(psi_2_stepNew).^2+abs(psi_3_stepNew).^2) + c2*(abs(psi_2_stepNew).^2+abs(psi_3_stepNew).^2-abs(psi_1_stepNew).^2))).*psi_3_stepNew;   % xj * yk
    
    % Fifth step
    [psi_1_stepNew,psi_2_stepNew,psi_3_stepNew] = PAPtransform(psi_1_step,psi_2_step,psi_3_step,c2,delta_t/2,0);    % xj * yk
    
    % Sixth step
    psi_hat1 = FFT_y_sine(psi_1_stepNew,Mx,Ny,y_k,la_q,cy); % Mx * qy
    psi_hat2 = FFT_y_sine(psi_2_stepNew,Mx,Ny,y_k,la_q,cy); % Mx * qy
    psi_hat3 = FFT_y_sine(psi_3_stepNew,Mx,Ny,y_k,la_q,cy); % Mx * qy
    
    psi_1_step = rotatingIFFT_y_mod(Omega,delta_t,psi_hat1,Mx,Ny,x_j,y_k,la_q,cy); % xj * yk
    psi_2_step = rotatingIFFT_y_mod(Omega,delta_t,psi_hat2,Mx,Ny,x_j,y_k,la_q,cy); % xj * yk
    psi_3_step = rotatingIFFT_y_mod(Omega,delta_t,psi_hat3,Mx,Ny,x_j,y_k,la_q,cy); % xj * yk
    
    % Seventh step --> advancing from t_n to t_n+1 --> end of one loop
    psi_hat1 = FFT_x_sine(psi_1_step,Mx,Ny,x_j,mu_p,ax); % Ny * px
    psi_hat2 = FFT_x_sine(psi_2_step,Mx,Ny,x_j,mu_p,ax); % Ny * px
    psi_hat3 = FFT_x_sine(psi_3_step,Mx,Ny,x_j,mu_p,ax); % Ny * px
    
    psi_1_step = rotatingIFFT_x_mod(Omega,delta_t,psi_hat1,Mx,Ny,x_j,y_k,mu_p,ax); % yk * xj
    psi_2_step = rotatingIFFT_x_mod(Omega,delta_t,psi_hat2,Mx,Ny,x_j,y_k,mu_p,ax); % yk * xj
    psi_3_step = rotatingIFFT_x_mod(Omega,delta_t,psi_hat3,Mx,Ny,x_j,y_k,mu_p,ax); % yk * xj
    
    psi_1 = psi_1_step.';
    psi_2 = psi_2_step.';
    psi_3 = psi_3_step.';
    
    
    N1(tt) = sum(sum(abs(psi_1).^2)*h)*h;
    N2(tt) = sum(sum(abs(psi_2).^2)*h)*h;
    N3(tt) = sum(sum(abs(psi_3).^2)*h)*h;
% %     psi_1 = psi_1/sqrt(N1(tt)+N2(tt)+N3(tt));
% %     psi_2 = psi_2/sqrt(N1(tt)+N2(tt)+N3(tt));
% %     psi_3 = psi_3/sqrt(N1(tt)+N2(tt)+N3(tt));
%     N1(tt)
%     N2(tt)
%     N3(tt)
    
% %     [gy1,gx1] = gradient(psi_1,h);
% %     [gy2,gx2] = gradient(psi_2,h);
% %     [gy3,gx3] = gradient(psi_3,h);
    rho_tot = abs(psi_1).^2 + abs(psi_2).^2 + abs(psi_3).^2;
    
        delta1(tt) = sum(sum(rho_tot .* (xx.^2 + yy.^2)))*h*h;
    %     delta2(tt) = sum(sum(abs(psi_2).^2 .* (xx.^2 + yy.^2)))*h*h;
    %     delta3(tt) = sum(sum(abs(psi_3).^2 .* (xx.^2 + yy.^2)))*h*h;
    
    figure(1); subplot(2,2,1); surf(y_k,x_j,abs(psi_1).^2); shading interp; view(0,90);
    figure(1); subplot(2,2,2); surf(y_k,x_j,abs(psi_2).^2); shading interp; view(0,90);
    figure(1); subplot(2,2,3); surf(y_k,x_j,abs(psi_3).^2); shading interp; view(0,90);
    figure(1); subplot(2,2,4); surf(y_k,x_j,rho_tot); shading interp; view(0,90);
end

figure(3); plot((1:tTotal)*delta_t,N1);
hold on; plot((1:tTotal)*delta_t,N2,'b:');
hold on; plot((1:tTotal)*delta_t,N3,'bx');
hold on; plot((1:tTotal)*delta_t,N1+N2+N3,'b--');


figure(4); plot((1:tTotal)*delta_t,delta1);
% hold on; plot((1:tTotal)*delta_t,delta2,'b:');
% hold on; plot((1:tTotal)*delta_t,delta3,'bx');
% hold on; plot((1:tTotal)*delta_t,delta1+delta2+delta3,'b--');