%% TSSP for 3 components in 2D
% Numerical Simulations on Stationary States for 3 component BECs
% from SIAM Vol. 30, No. 4, pp. 1925-1948 --
% This program is under construction, colorbar is not scaled.
% Benchmark to Fig 12, section 4.3 page 1939 to 1940
% also c.f. to 2009 PRE 78 reference for beta vs c0/c2

clear all; close all; clc;

om_trap = 2*pi*20;
hbar = 1.0545717e-34;
mRb = 1.443e-25;
a0 = sqrt(hbar/mRb/om_trap);
k_rec = 0;
Natom = 10000;
Omega = 0;
Raman = 0;
Mg = 0.5;
N0 = 0.02;
q_field = 0.1;

gamma_x = 1;
gamma_y = 1;

delta_t = -1i*0.01*0.05;	% time step interval

ax = -10; bx = 10;    % xmin and xmax
cy = -10; dy = 10;    % ymin and ymax

h = 1/8;           % h is the mesh size: h = (bx-ax)/Mx = (dy-cy)/Ny
Mx = round((bx-ax)/h);
Ny = round((dy-cy)/h);

x_j = (ax+h):h:(bx-h);
y_k = (cy+h):h:(dy-h);

% FFT summation indices for x and y spaces:
px = (-Mx/2+1):(Mx/2-1);
qy = (-Ny/2+1):(Ny/2-1);

% FFT reciprocal space
mu_p = 2*pi*px/(bx-ax);
la_q = 2*pi*qy/(dy-cy);

[yy,xx] = meshgrid(y_k,x_j);

% Initial wavefunction
psi_ho = (gamma_x*gamma_y)^(1/4)/sqrt(pi).*exp(-(gamma_x*xx.^2+gamma_y*yy.^2)/2);   % xj * yk
psi_cc = psi_ho;
norm_fac = sum(sum(abs(psi_cc).^2)*h)*h;

psi_1 = psi_cc/sqrt(2*norm_fac)*sqrt(1+Mg-N0);
psi_2 = psi_cc/sqrt(norm_fac)*sqrt(N0);
psi_3 = psi_cc/sqrt(2*norm_fac)*sqrt(1-Mg-N0);


tTotal = round(-1i*15/delta_t);
N1 = zeros(1,tTotal);
N2 = zeros(1,tTotal);
N3 = zeros(1,tTotal);

delta1 = zeros(1,tTotal);
delta2 = zeros(1,tTotal);
delta3 = zeros(1,tTotal);


% --87Rb figure left
c0 = 885.4;
c2 = -4.1;

% --23Na figure right
% c0 = 240.8;
% c2 = 7.5;

V0 = 100;
V2D_1 = 1/2*(gamma_x^2*xx.^2 + gamma_y^2*yy.^2) + V0*(sin(pi/2*xx).^2+sin(pi/2*yy).^2); % xj * yk


for tt = 1:tTotal
    
    % First step
    psi_hat1 = FFT_x_sine(psi_1,Mx,Ny,x_j,mu_p,ax);
    psi_hat2 = FFT_x_sine(psi_2,Mx,Ny,x_j,mu_p,ax);
    psi_hat3 = FFT_x_sine(psi_3,Mx,Ny,x_j,mu_p,ax);

    psi_1_step = rotatingIFFT_x_mod(Omega,delta_t,psi_hat1,Mx,Ny,x_j,y_k,mu_p,ax);
    psi_2_step = rotatingIFFT_x_mod(Omega,delta_t,psi_hat2,Mx,Ny,x_j,y_k,mu_p,ax);
    psi_3_step = rotatingIFFT_x_mod(Omega,delta_t,psi_hat3,Mx,Ny,x_j,y_k,mu_p,ax);
    
    
    % Second step
    psi_hat1 = FFT_y_sine(psi_1_step.',Mx,Ny,y_k,la_q,cy); % Mx * qy
    psi_hat2 = FFT_y_sine(psi_2_step.',Mx,Ny,y_k,la_q,cy); % Mx * qy
    psi_hat3 = FFT_y_sine(psi_3_step.',Mx,Ny,y_k,la_q,cy); % Mx * qy
    
    psi_1_step = rotatingIFFT_y_mod(Omega,delta_t,psi_hat1,Mx,Ny,x_j,y_k,la_q,cy); % xj * yk
    psi_2_step = rotatingIFFT_y_mod(Omega,delta_t,psi_hat2,Mx,Ny,x_j,y_k,la_q,cy); % xj * yk
    psi_3_step = rotatingIFFT_y_mod(Omega,delta_t,psi_hat3,Mx,Ny,x_j,y_k,la_q,cy); % xj * yk
    
    % Second step
    [psi_1_stepNew,psi_2_stepNew,psi_3_stepNew] = PAPtransform(psi_1_step,psi_2_step,psi_3_step,c2,delta_t/2,0);    % xj * yk
    
    % Third step
    psi_1_step = exp(-1i*delta_t*(V2D_1 + q_field + c0*(abs(psi_1_stepNew).^2+abs(psi_2_stepNew).^2+abs(psi_3_stepNew).^2) + c2*(abs(psi_1_stepNew).^2+abs(psi_2_stepNew).^2-abs(psi_3_stepNew).^2))).*psi_1_stepNew;   % xj * yk
    psi_2_step = exp(-1i*delta_t*(V2D_1 + c0*(abs(psi_1_stepNew).^2+abs(psi_2_stepNew).^2+abs(psi_3_stepNew).^2) + c2*(abs(psi_1_stepNew).^2+abs(psi_3_stepNew).^2))).*psi_2_stepNew;   % xj * yk
    psi_3_step = exp(-1i*delta_t*(V2D_1 + q_field + c0*(abs(psi_1_stepNew).^2+abs(psi_2_stepNew).^2+abs(psi_3_stepNew).^2) + c2*(abs(psi_2_stepNew).^2+abs(psi_3_stepNew).^2-abs(psi_1_stepNew).^2))).*psi_3_stepNew;   % xj * yk
    
    % Fourth step
    [psi_1_stepNew,psi_2_stepNew,psi_3_stepNew] = PAPtransform(psi_1_step,psi_2_step,psi_3_step,c2,delta_t/2,0);    % xj * yk
    
    % Sixth step
    psi_hat1 = FFT_y_sine(psi_1_stepNew,Mx,Ny,y_k,la_q,cy); % Mx * qy
    psi_hat2 = FFT_y_sine(psi_2_stepNew,Mx,Ny,y_k,la_q,cy); % Mx * qy
    psi_hat3 = FFT_y_sine(psi_3_stepNew,Mx,Ny,y_k,la_q,cy); % Mx * qy
    
    psi_1_step = rotatingIFFT_y_mod(Omega,delta_t,psi_hat1,Mx,Ny,x_j,y_k,la_q,cy); % xj * yk
    psi_2_step = rotatingIFFT_y_mod(Omega,delta_t,psi_hat2,Mx,Ny,x_j,y_k,la_q,cy); % xj * yk
    psi_3_step = rotatingIFFT_y_mod(Omega,delta_t,psi_hat3,Mx,Ny,x_j,y_k,la_q,cy); % xj * yk
    
    % Seventh step --> advancing from t_n to t_n+1 --> end of one loop
    psi_hat1 = FFT_x_sine(psi_1_step,Mx,Ny,x_j,mu_p,ax); % Ny * px
    psi_hat2 = FFT_x_sine(psi_2_step,Mx,Ny,x_j,mu_p,ax); % Ny * px
    psi_hat3 = FFT_x_sine(psi_3_step,Mx,Ny,x_j,mu_p,ax); % Ny * px
    
    psi_1_step = rotatingIFFT_x_mod(Omega,delta_t,psi_hat1,Mx,Ny,x_j,y_k,mu_p,ax); % yk * xj
    psi_2_step = rotatingIFFT_x_mod(Omega,delta_t,psi_hat2,Mx,Ny,x_j,y_k,mu_p,ax); % yk * xj
    psi_3_step = rotatingIFFT_x_mod(Omega,delta_t,psi_hat3,Mx,Ny,x_j,y_k,mu_p,ax); % yk * xj
        
    rho1 = sum(sum(abs(psi_1_step).^2))*h*h;
    rho2 = sum(sum(abs(psi_2_step).^2))*h*h;
    rho3 = sum(sum(abs(psi_3_step).^2))*h*h;
    
    sig_proj2 = sqrt(1-Mg^2)/sqrt(rho2 + sqrt(4*(1-Mg^2)*rho1*rho3 + Mg^2*rho2^2));
    sig_proj1 = sqrt(1+Mg-sig_proj2^2*rho2)/sqrt(2*rho1);
    sig_proj3 = sqrt(1-Mg-sig_proj2^2*rho2)/sqrt(2*rho3);
%     
%     if rho1 == 0
%         sig_proj1 = 1;
%     end
%     if rho3 == 0
%         sig_proj3 = 1;
%     end
%     sig_proj1 = 1; sig_proj2 = 1; sig_proj3 = 1;
    psi_1 = sig_proj1*psi_1_step.';
    psi_2 = sig_proj2*psi_2_step.';
    psi_3 = sig_proj3*psi_3_step.';
    
    N1(tt) = sum(sum(abs(psi_1).^2))*h*h;
    N2(tt) = sum(sum(abs(psi_2).^2))*h*h;
    N3(tt) = sum(sum(abs(psi_3).^2))*h*h;
    rho_tot = abs(psi_1).^2 + abs(psi_2).^2 + abs(psi_3).^2;

        
    figure(1); subplot(2,2,1); surf(y_k,x_j,abs(psi_1).^2); shading interp; view(0,90);
    figure(1); subplot(2,2,2); surf(y_k,x_j,abs(psi_2).^2); shading interp; view(0,90);
    figure(1); subplot(2,2,3); surf(y_k,x_j,abs(psi_3).^2); shading interp; view(0,90);
    figure(1); subplot(2,2,4); surf(y_k,x_j,rho_tot); shading interp; view(0,90);
end

figure; plot((1:tTotal)*1i*delta_t,N1);
hold on; plot((1:tTotal)*1i*delta_t,N2,'b:');
hold on; plot((1:tTotal)*1i*delta_t,N3,'bx');
hold on; plot((1:tTotal)*1i*delta_t,N1+N2+N3,'b--');
% 
% figure; plot(x_j,abs(psi_1));
% hold on; plot(x_j,abs(psi_2),'ro');
% hold on; plot(x_j,abs(psi_3),'kv');
% hold on; plot((1:tTotal)*delta_t,delta2,'b:');
% hold on; plot((1:tTotal)*delta_t,delta3,'bx');
% hold on; plot((1:tTotal)*delta_t,delta1+delta2+delta3,'b--');