function [psi_1_stepNew,psi_2_stepNew,psi_3_stepNew] = PAPtransform(psi_1,psi_0,psi_m1,c2,delta_t,B)

    b12 = c2*psi_0.*conj(psi_m1)+B;
    b23 = c2*psi_1.*conj(psi_0)+B;

    psi_1_1 = psi_1 - 1i*delta_t*b12.*psi_0;
    psi_2_1 = psi_0 - 1i*delta_t*(conj(b12).*psi_1 + b23.*psi_m1);
    psi_3_1 = psi_m1 - 1i*delta_t*conj(b23).*psi_0;

    b12 = c2/2*(psi_0.*conj(psi_m1)+psi_2_1.*conj(psi_3_1))+B;
    b23 = c2/2*(psi_1.*conj(psi_0)+psi_1_1.*conj(psi_2_1))+B;
    alph = sqrt(abs(b12).^2+abs(b23).^2);

    p12al = b12./alph/sqrt(2);
%     p12al(isnan(p12al)) = 1;

    p23al = b23./alph/sqrt(2);
%     p23al(isnan(p23al)) = 1;
    if ~isnan(p12al)
        psi_1_1 = sqrt(2)*conj(p23al).*psi_1 - sqrt(2)*p12al.*psi_m1;
        psi_2_1 = conj(p12al).*psi_1 + 1/sqrt(2)*psi_0 + p23al.*psi_m1;
        psi_3_1 = -conj(p12al).*psi_1 + 1/sqrt(2)*psi_0 - p23al.*psi_m1;

        psi_1_t = psi_1_1;
        psi_0_t = exp(-1i*delta_t*alph).*psi_2_1;
        psi_m1_t = exp(1i*delta_t*alph).*psi_3_1;

        psi_1_stepNew = sqrt(2)*p23al.*psi_1_t + p12al.*psi_0_t - p12al.*psi_m1_t;
        psi_2_stepNew = 1/sqrt(2)*psi_0_t + 1/sqrt(2)*psi_m1_t;
        psi_3_stepNew = -sqrt(2)*conj(p12al).*psi_1_t + conj(p23al).*psi_0_t - conj(p23al).*psi_m1_t;
    else
        psi_1_stepNew = psi_1;
        psi_2_stepNew = psi_0;
        psi_3_stepNew = psi_m1;
    end

end