function [psi] = rotatingIFFT_x(kRec,dt,psi_hat,Mx,Ny,x,y,mu,a)
    % matrix size of psi_hat: Ny * px
    % matrix size of psi: yk * xj
    
    PDEmat = exp(-1i*dt/4* (repmat(mu.^2,Ny+1,1)+2*kRec*repmat(mu,Ny+1,1)) );
    psi = (PDEmat.*psi_hat) * exp(1i* mu.'*(x-a));

end