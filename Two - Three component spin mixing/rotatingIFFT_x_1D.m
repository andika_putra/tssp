function [psi] = rotatingIFFT_x_1D(dt,psi_hat,x,mu,a)
    % matrix size of psi_hat: Ny * px
    % matrix size of psi: yk * xj
    
    PDEmat = exp(-1i*dt/4*(mu.^2));
    psi = (PDEmat.*psi_hat) * exp(1i* mu.'*(x-a));

end