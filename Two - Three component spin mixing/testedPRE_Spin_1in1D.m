%% TSSP for 3 components in 1D
% Numerical methods for spin-1 ground state in uniform magnetic field
% PRE 78, 066704 (2008) -- 1D three components
% Benchmark to figure 1 and 3 for 87Rb and 23Na in harmonic /and lattice
% potential

clear all; close all; clc;

om_trap = 2*pi*20;
hbar = 1.0545717e-34;
mRb = 1.443e-25;
a0 = sqrt(hbar/mRb/om_trap);
k_rec = 0;
Natom = 10000;
Omega = 0.9;
Raman = 0;
Mg = 0.3;
N0 = 0.7;
q_field = 0.1;
gamma_x = 1;

delta_t = -1i*0.01*0.1;	% time step interval

ax = -14; bx = 14;    % xmin and xmax

h = 1/10;           % h is the mesh size: h = (bx-ax)/Mx = (dy-cy)/Ny
Mx = round((bx-ax)/h);

x_j = (ax+h):h:(bx-h);

% FFT summation indices for x and y spaces:
px = (-Mx/2+1):(Mx/2-1);

% FFT reciprocal space
mu_p = 2*pi*px/(bx-ax);

% Initial wavefunction
psi_ho = (gamma_x)^(1/2)/sqrt(sqrt(pi)).*exp(-gamma_x*x_j.^2/2);   % xj * yk
psi_cc = psi_ho;
norm_fac = sum(abs(psi_cc).^2)*h;

psi_1 = psi_cc/sqrt(2*norm_fac)*sqrt(1+Mg-N0);
psi_2 = psi_cc/sqrt(norm_fac)*sqrt(N0);
psi_3 = psi_cc/sqrt(2*norm_fac)*sqrt(1-Mg-N0);


tTotal = round(-1i*15/delta_t);
N1 = zeros(1,tTotal);
N2 = zeros(1,tTotal);
N3 = zeros(1,tTotal);

delta1 = zeros(1,tTotal);
delta2 = zeros(1,tTotal);
delta3 = zeros(1,tTotal);

% --87Rb figure 1
c0 = 885.4;
c2 = -4.1;

% --23Na figure 3
% c0 = 240.8;
% c2 = 7.5;

V0 = 50;
% V0 = 0;
V2D_1 = 1/2*(gamma_x^2*x_j.^2)+V0*sin(pi/2*x_j).^2;

for tt = 1:tTotal
    
    % First step
    psi_hat1 = FFT_x_1D(psi_1,Mx,x_j,mu_p,ax);
    psi_hat2 = FFT_x_1D(psi_2,Mx,x_j,mu_p,ax);
    psi_hat3 = FFT_x_1D(psi_3,Mx,x_j,mu_p,ax);

    psi_1_step = rotatingIFFT_x_1D(delta_t,psi_hat1,x_j,mu_p,ax);
    psi_2_step = rotatingIFFT_x_1D(delta_t,psi_hat2,x_j,mu_p,ax);
    psi_3_step = rotatingIFFT_x_1D(delta_t,psi_hat3,x_j,mu_p,ax);
    
    % Second step
    [psi_1_stepNew,psi_2_stepNew,psi_3_stepNew] = PAPtransform(psi_1_step,psi_2_step,psi_3_step,c2,delta_t/2,0);    % xj * yk
    
    % Third step
    psi_1_step = exp(-1i*delta_t*(V2D_1 + q_field + c0*(abs(psi_1_stepNew).^2+abs(psi_2_stepNew).^2+abs(psi_3_stepNew).^2) + c2*(abs(psi_1_stepNew).^2+abs(psi_2_stepNew).^2-abs(psi_3_stepNew).^2))).*psi_1_stepNew;   % xj * yk
    psi_2_step = exp(-1i*delta_t*(V2D_1 + c0*(abs(psi_1_stepNew).^2+abs(psi_2_stepNew).^2+abs(psi_3_stepNew).^2) + c2*(abs(psi_1_stepNew).^2+abs(psi_3_stepNew).^2))).*psi_2_stepNew;   % xj * yk
    psi_3_step = exp(-1i*delta_t*(V2D_1 + q_field + c0*(abs(psi_1_stepNew).^2+abs(psi_2_stepNew).^2+abs(psi_3_stepNew).^2) + c2*(abs(psi_2_stepNew).^2+abs(psi_3_stepNew).^2-abs(psi_1_stepNew).^2))).*psi_3_stepNew;   % xj * yk
    
    % Fourth step
    [psi_1_stepNew,psi_2_stepNew,psi_3_stepNew] = PAPtransform(psi_1_step,psi_2_step,psi_3_step,c2,delta_t/2,0);    % xj * yk
    
    % Fifth step
    psi_hat1 = FFT_x_1D(psi_1_stepNew,Mx,x_j,mu_p,ax); % Ny * px
    psi_hat2 = FFT_x_1D(psi_2_stepNew,Mx,x_j,mu_p,ax); % Ny * px
    psi_hat3 = FFT_x_1D(psi_3_stepNew,Mx,x_j,mu_p,ax); % Ny * px

    psi_1_step = rotatingIFFT_x_1D(delta_t,psi_hat1,x_j,mu_p,ax); % yk * xj
    psi_2_step = rotatingIFFT_x_1D(delta_t,psi_hat2,x_j,mu_p,ax); % yk * xj
    psi_3_step = rotatingIFFT_x_1D(delta_t,psi_hat3,x_j,mu_p,ax); % yk * xj
    
    rho1 = sum(abs(psi_1_step).^2)*h;
    rho2 = sum(abs(psi_2_step).^2)*h;
    rho3 = sum(abs(psi_3_step).^2)*h;
    
    sig_proj2 = sqrt(1-Mg^2)/sqrt(rho2 + sqrt(4*(1-Mg^2)*rho1*rho3 + Mg^2*rho2^2));
    sig_proj1 = sqrt(1+Mg-sig_proj2^2*rho2)/sqrt(2*rho1);
    sig_proj3 = sqrt(1-Mg-sig_proj2^2*rho2)/sqrt(2*rho3);
    
    psi_1 = sig_proj1*psi_1_step;
    psi_2 = sig_proj2*psi_2_step;
    psi_3 = sig_proj3*psi_3_step;
    
    
    N1(tt) = sum(abs(psi_1).^2)*h;
    N2(tt) = sum(abs(psi_2).^2)*h;
    N3(tt) = sum(abs(psi_3).^2)*h;
    
    figure(1); plot(x_j,abs(psi_1).^2,'b--');
    hold on; plot(x_j,abs(psi_2).^2,'g');
    hold on; plot(x_j,abs(psi_3).^2,'r-.');
    hold off;
%     rho_tot = abs(psi_1).^2 + abs(psi_2).^2 + abs(psi_3).^2;
    
% 	delta1(tt) = sum(rho_tot .* x_j.^2)*h;
    %     delta2(tt) = sum(sum(abs(psi_2).^2 .* (xx.^2 + yy.^2)))*h*h;
    %     delta3(tt) = sum(sum(abs(psi_3).^2 .* (xx.^2 + yy.^2)))*h*h;
    
%     figure(1); subplot(2,2,1); plot(x_j,abs(psi_1).^2);
%     figure(1); subplot(2,2,2); plot(x_j,abs(psi_2).^2);
%     figure(1); subplot(2,2,3); plot(x_j,abs(psi_3).^2);
%     figure(1); subplot(2,2,4); plot(x_j,rho_tot);
end

figure; plot((1:tTotal)*1i*delta_t,N1);
hold on; plot((1:tTotal)*1i*delta_t,N2,'b:');
hold on; plot((1:tTotal)*1i*delta_t,N3,'bx');
hold on; plot((1:tTotal)*1i*delta_t,N1+N2+N3,'b--');
% 
% figure; plot(x_j,abs(psi_1));
% hold on; plot(x_j,abs(psi_2),'ro');
% hold on; plot(x_j,abs(psi_3),'kv');
% hold on; plot((1:tTotal)*delta_t,delta2,'b:');
% hold on; plot((1:tTotal)*delta_t,delta3,'bx');
% hold on; plot((1:tTotal)*delta_t,delta1+delta2+delta3,'b--');